#!/usr/bin/env bash

function list_qml_files() {
    local all_qmls=$(find ./src/ -name "*.qml")

    for file in $all_qmls; do
        if ! [ -z "$(grep -E '^import com.togg.can' "$file")" ]; then
            echo "$file"
        fi
    done
}

function find_suspicious_enum_occurrences() {
    local enum_name=$1
    local enum_field=$2
    
    cat dds_qml_files.txt \
        | xargs grep -H -E "\\.\\s*$enum_field([^a-zA-Z0-9_]|$)" \
        | grep -v -E '(CanEnums|Can|DdsEnums)\s*\.\s*'"$enum_name"'\s*\.\s*'"$enum_field"'([^a-zA-Z0-9]|$)'
}
export -f find_suspicious_enum_occurrences

function find_all_enum_fields() {
    local enum_name=$1

    find -name '*.idl' \
        | xargs -n1 grep -ho -E 'value\([0-9]+\) '"${enum_name}"'_[a-zA-Z0-9_]+' \
        | sed -E 's/^.*value\([0-9]+\) '"${enum_name}"'_([a-zA-Z0-9_]+)(,)?$/'"${enum_name}"' \1/' \
        | sed -E 's/'"${enum_name}"' ([0-9]+[a-zA-Z0-9_]*)$/'"${enum_name}"' Enum\1/' \
        | sort -u
}
export -f find_all_enum_fields

function find_all_enums() {
    find src/dds/generated/ -name '*.idl' \
        | xargs cat \
        | grep -E ' *enum .*_t' \
        | sed -E 's/^\s*enum ([A-Za-z0-9_]+)_t?( )*$/\1/' \
        | sort -u 
}

function filter_results_with_blacklist() {
    cp result.txt filtered_result.txt
    while IFS= read -r blacklisted_string; do        
        cat filtered_result.txt | grep -v -F "$blacklisted_string" > tmp.txt
        mv tmp.txt filtered_result.txt
    done < blacklist.txt
}

list_qml_files > dds_qml_files.txt
find_all_enums > enums.txt
cat enums.txt | xargs -I '{}' bash -c 'find_all_enum_fields {}' > enums_fields.txt
cat enums_fields.txt | xargs -d '\n' -I '{}' bash -c 'find_suspicious_enum_occurrences {}' > result.txt

filter_results_with_blacklist > filtered_result.txt
